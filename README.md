A Chrome extension to show the current price and 24h change in value for the cryptocurrency BAT against 40+ fiat currencies.

Data is sourced from [CoinGecko's data API](https://www.coingecko.com/en/api), and is updated every minute.

Official release can be found [on the Chrome Web Store](https://chrome.google.com/webstore/detail/fpgnkcakakiminoablpndllfbhobglid).
