/*global chrome */


import {CONFIG, getCurrency, formatValue} from './common.js';

const conf = {
    method: 'GET',
    mode: 'cors'
};


/**
 * Get the supported currencies from the API
 */
function getSupportedCurrencies() {
    fetch('https://api.coingecko.com/api/v3/simple/supported_vs_currencies', conf)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Response ${response.status}: ${response.statusText}`);
            }

            return response.json();
        })
        .then(data => {
            const currencies = data.join(',');

            chrome.storage.local.set({currencies: currencies});
        })
        .catch(error => {
            console.error('Unable to fetch API data:', error);
        });
}


/**
 * Update the extension badge
 */
function updateBadge() {
    chrome.storage.local.get({
        price: 0,
        change: 0,
        lastUpdated: 0,
        currency: 'usd'
    }, (data) => {
        const now = Date.now();

        if((now - parseInt(data.lastUpdated, 10)) > CONFIG.CONNECTION_TIMEOUT) {
            chrome.action.setBadgeBackgroundColor({color: CONFIG.COLOUR.DISCONNECTED});
        } else {
            if(data.change < 0) {
                chrome.action.setBadgeBackgroundColor({color: CONFIG.COLOUR.NEGATIVE});
            } else {
                chrome.action.setBadgeBackgroundColor({color: CONFIG.COLOUR.POSITIVE});
            }

            chrome.action.setBadgeText({text: formatValue(data.price, true)});

            const currency = getCurrency(data.currency);
            const currentPrice = `${currency.localFormat.replace('{}', formatValue(data.price))}`;

            chrome.action.setTitle({title: currentPrice});
        }
    });
}


/**
 * Call the API to get the server and billing data
 */
function getAPIData() {
    chrome.storage.local.get({
        currency: 'usd',
        lastUpdated: 0
    }, (config) => {
        fetch(`https://api.coingecko.com/api/v3/simple/price?ids=${CONFIG.TOKEN}&vs_currencies=${config.currency}&include_24hr_change=true&__bt=${config.lastUpdated}`, conf)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`Response ${response.status}: ${response.statusText}`);
                }

                return response.json();
            })
            .then(data => {
                if(data['basic-attention-token']) {
                    const value = data[CONFIG.TOKEN][config.currency];
                    const change = data[CONFIG.TOKEN][`${config.currency}_24h_change`];
                    const now = Date.now();

                    chrome.storage.local.set({price: value, change: change, lastUpdated: now});
                } else {
                    throw new Error(`API returned no data for currency ${config.currency}`);
                }
            })
            .catch(error => {
                console.error('Unable to fetch API data:', error);
            })
            .finally(() => {
                updateBadge();
            });
    });
}


/**
 * Set the update timers
 */
function init() { //eslint-disable-line no-unused-vars
    getSupportedCurrencies();
    getAPIData();

    chrome.alarms.clear('BATTickerTimer');
    chrome.alarms.clear('CurrencyUpdateTimer');

    chrome.alarms.create('BATTickerTimer', {delayInMinutes: CONFIG.PRICE_UPDATE.DELAY, periodInMinutes: CONFIG.PRICE_UPDATE.PERIOD});
    chrome.alarms.create('CurrencyUpdateTimer', {delayInMinutes: CONFIG.CURRENCY_UPDATE.DELAY, periodInMinutes: CONFIG.CURRENCY_UPDATE.PERIOD});
}


chrome.runtime.onStartup.addListener(() => {
    init();
});


chrome.runtime.onInstalled.addListener(() => {
    init();
});


chrome.alarms.onAlarm.addListener(alarm => {
    if(alarm.name === 'BATTickerTimer') {
        getAPIData();
    } else if(alarm.name === 'CurrencyUpdateTimer') {
        getSupportedCurrencies();
    }
});


chrome.runtime.onMessage.addListener(request => {
    if(request.action === 'update settings') {
        getAPIData();
    }

    return true;
});
