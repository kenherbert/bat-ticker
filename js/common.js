export const CURRENCY = { //eslint-disable-line no-unused-vars
    aed: {
        name: 'United Arab Emirates dirham',
        symbol: 'DH',
        localFormat: 'DH{}'
    },
    ars: {
        name: 'Argentine peso',
        symbol: '$',
        localFormat: '${}'
    },
    aud: {
        name: 'Australian dollar',
        symbol: '$',
        localFormat: '${}'
    },
    bdt: {
        name: 'Bangladeshi taka',
        symbol: `${String.fromCodePoint(2547)}`,
        localFormat: `${String.fromCodePoint(2547)}{}`
    },
    bhd: {
        name: 'Bahraini dinar',
        symbol: 'BD',
        localFormat: 'BD{}'
    },
    bmd: {
        name: 'Bermudian dollar',
        symbol: '$',
        localFormat: '${}'
    },
    brl: {
        name: 'Brazilian real',
        symbol: 'R$',
        localFormat: 'R${}'
    },
    cad: {
        name: 'Canadian dollar',
        symbol: '$',
        localFormat: '${}'
    },
    chf: {
        name: 'Swiss franc',
        symbol: 'CHF',
        localFormat: 'CHF{}'
    },
    clp: {
        name: 'Chilean peso',
        symbol: '$',
        localFormat: '${}'
    },
    cny: {
        name: 'Chinese yuan',
        symbol: `${String.fromCodePoint(165)}`,
        localFormat: `${String.fromCodePoint(165)}{}`
    },
    czk: {
        name: 'Czech koruna',
        symbol: `K${String.fromCodePoint(269)}`,
        localFormat: `{}K${String.fromCodePoint(269)}`
    },
    dkk: {
        name: 'Danish krone',
        symbol: 'kr.',
        localFormat: '{} kr.'
    },
    eur: {
        name: 'Euro',
        symbol: `${String.fromCodePoint(8364)}`,
        localFormat: `${String.fromCodePoint(8364)}{}`
    },
    gbp: {
        name: 'Pounds sterling',
        symbol: `${String.fromCodePoint(163)}`,
        localFormat: `${String.fromCodePoint(163)}{}`
    },
    hkd: {
        name: 'Hong Kong dollar',
        symbol: '$',
        localFormat: '${}'
    },
    huf: {
        name: 'Hungarian forint',
        symbol: 'Ft',
        localFormat: '{} Ft'
    },
    idr: {
        name: 'Indonesian rupiah',
        symbol: 'Rp',
        localFormat: 'Rp{}'
    },
    ils: {
        name: 'Israeli shekel',
        symbol: `${String.fromCodePoint(8362)}`,
        localFormat: `${String.fromCodePoint(8362)}{}`
    },
    inr: {
        name: 'Indian rupee',
        symbol: `${String.fromCodePoint(8377)}`,
        localFormat: `${String.fromCodePoint(8377)}{}`
    },
    jpy: {
        name: 'Japanese yen',
        symbol: `${String.fromCodePoint(165)}`,
        localFormat: `${String.fromCodePoint(165)}{}`
    },
    krw: {
        name: 'South Korean won',
        symbol: `${String.fromCodePoint(8361)}`,
        localFormat: `${String.fromCodePoint(8361)}{}`
    },
    kwd: {
        name: 'Kuwaiti dinar',
        symbol: 'KD',
        localFormat: '{} KD'
    },
    lkr: {
        name: 'Sri Lankan rupee',
        symbol: `${String.fromCodePoint(8360)}.`,
        localFormat: `${String.fromCodePoint(8360)}.{}`
    },
    mmk: {
        name: 'Myanmar kyat',
        symbol: 'K',
        localFormat: 'K{}'
    },
    mxn: {
        name: 'Mexican peso',
        symbol: '$',
        localFormat: '${}'
    },
    myr: {
        name: 'Malaysian ringgit',
        symbol: 'RM',
        localFormat: 'RM{}'
    },
    ngn: {
        name: 'Nigerian naira',
        symbol: `${String.fromCodePoint(8358)}`,
        localFormat: `${String.fromCodePoint(8358)}{}`
    },
    nkr: {
        name: 'Norwegian krone',
        symbol: 'kr',
        localFormat: '{} kr'
    },
    nzd: {
        name: 'New Zealand dollar',
        symbol: '$',
        localFormat: '${}'
    },
    php: {
        name: 'Philippine peso',
        symbol: `${String.fromCodePoint(8369)}`,
        localFormat: `${String.fromCodePoint(8369)}{}`
    },
    pkr: {
        name: 'Pakistani rupee',
        symbol: `${String.fromCodePoint(8360)}`,
        localFormat: `${String.fromCodePoint(8360)} {}`
    },
    pln: {
        name: 'Polish zloty',
        symbol: `z${String.fromCodePoint(322)}`,
        localFormat: `{} z${String.fromCodePoint(322)}`
    },
    rub: {
        name: 'Russian Ruble',
        symbol: `${String.fromCodePoint(8381)}`,
        localFormat: `${String.fromCodePoint(8381)}{}`
    },
    sar: {
        name: 'Saudi riyal',
        symbol: `${String.fromCodePoint(65020)}`,
        localFormat: `{}${String.fromCodePoint(65020)}`
    },
    sek: {
        name: 'Swedish krona',
        symbol: 'kr',
        localFormat: '{} kr'
    },
    sgd: {
        name: 'Singaporean dollar',
        symbol: '$',
        localFormat: '${}'
    },
    thb: {
        name: 'Thai baht',
        symbol: `${String.fromCodePoint(3647)}`,
        localFormat: `${String.fromCodePoint(3647)}{}`
    },
    try: {
        name: 'Turkish lira',
        symbol: `${String.fromCodePoint(8378)}`,
        localFormat: `${String.fromCodePoint(8378)}{}`
    },
    twd: {
        name: 'New Taiwan dollar',
        symbol: 'NT$',
        localFormat: 'NT${}'
    },
    uah: {
        name: 'Ukrainian hryvnia',
        symbol: `${String.fromCodePoint(8372)}`,
        localFormat: `${String.fromCodePoint(8372)}{}`
    },
    vef: {
        name: 'Venezualan bolivar',
        symbol: 'Bs.',
        localFormat: 'Bs.{}'
    },
    vnd: {
        name: 'Vietnamese dong',
        symbol: `${String.fromCodePoint(8363)}`,
        localFormat: `{}${String.fromCodePoint(8363)}`
    },
    zar: {
        name: 'South African rand',
        symbol: 'R',
        localFormat: 'R {}'
    },
    usd: {
        name: 'US dollar',
        symbol: '$',
        localFormat: '${}'
    }
};


export const CONFIG = { //eslint-disable-line no-unused-vars
    TOKEN: 'basic-attention-token',
    COLOUR: {
        POSITIVE: '#009900',
        NEGATIVE: '#990000',
        DISCONNECTED: '#555555'
    },
    PRICE_UPDATE: {
        DELAY: 1, // 1 minute
        PERIOD: 1 // 1 minute
    },
    CURRENCY_UPDATE: {
        DELAY: 60, // * 12, // 12 hours
        PERIOD: 60 // * 12 // 12 hours
    },
    CONNECTION_TIMEOUT: 60 * 5 * 1000 // 5 minutes in milliseconds
};


/**
 * Get the currency from the short code
 * @param {String} name The name of the currency to find
 */
export function getCurrency(name) { //eslint-disable-line no-unused-vars
    if(CURRENCY[name]) {
        return CURRENCY[name];
    }

    return false;
}


/**
 * Format a number for display
 *
 * TODO: Make sure numbers that are near the top of a category display correctly
 *
 * @param {Number} number The value to format
 * @returns String
 */
export function formatValue(number, shorten) { // eslint-disable-line no-unused-vars
    if(typeof number !== 'number') {
        return '0.00';
    }

    let formattedNumber = number;

    if(shorten && number > 100000) {
        formattedNumber = (number / 1000).toFixed(0) + 'k';
    } else if(shorten && number > 1000) {
        formattedNumber = (Math.round(number / 100) / 10).toFixed(1) + 'k';
    } else if(shorten && number > 100) {
        formattedNumber = number.toFixed(0);
    } else if(shorten && number > 10) {
        formattedNumber = number.toFixed(1);
    } else {
        formattedNumber = number.toFixed(2);
    }
    return formattedNumber;
}
