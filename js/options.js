/*global chrome */

import {getCurrency} from './common.js';

/**
 * Populate the user preferences from storage
 */
function populateUserPrefs() {
    chrome.storage.local.get({
        currency: 'usd',
        currencies: 'usd'
    }, (prefs) => {
        const currencySelect = document.getElementById('currency');
        let currencies = prefs.currencies.split(',');

        let currencyOptions = currencies.filter(currencyName => {
            return getCurrency(currencyName) !== false;
        }).map(currencyName => {
            const currency = getCurrency(currencyName);

            return `<option value="${currencyName}">${currency.name} (${currency.symbol})</option>`;
        });

        currencySelect.innerHTML = currencyOptions;
        currencySelect.value = prefs.currency;
    });
}


/**
 * Save user preferences to storage
 */
function saveUserPrefs() {
    var currency = document.getElementById('currency').value;

    chrome.storage.local.set({
        currency: currency
    }, () => {
        chrome.runtime.sendMessage({action: 'update settings'});
        var status = document.getElementById('status');

        status.textContent = 'Options saved.';

        setTimeout(() => {
            status.textContent = '';
        }, 750);
    });
}


/**
 * Initialise event handlers
 */
function initOptions() {
    document.addEventListener('DOMContentLoaded', populateUserPrefs);

    document.getElementById('save').addEventListener('click', saveUserPrefs);
}


// Prevent this from running when testing
if(!window.jasmine) {
    initOptions();
}
