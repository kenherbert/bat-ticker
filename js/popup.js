/*global chrome */

import {getCurrency, formatValue} from './common.js';

/**
 * Update the UI
 */
function updateUI() {
    chrome.storage.local.get({
        price: 0,
        change: 0,
        currency: 'usd',
        lastUpdated: 0
    }, (data) => {
        const currency = getCurrency(data.currency);
        document.getElementById('current-price').innerText = `${currency.localFormat.replace('{}', formatValue(data.price))}`;

        const prefix = (data.change > 0 ? '+' : '');
        document.getElementById('24h-change').innerText = `${prefix}${formatValue(data.change)}%`;

        const updatedDate = new Date(parseInt(data.lastUpdated, 10));
        document.getElementById('last-updated').innerText = updatedDate.toLocaleString();
    });
}


/**
 * Handle clicking the settings button
 */
function openSettings() {
    chrome.runtime.openOptionsPage();
}

/**
 * Initialise event handlers
 */
function initPopup() { // eslint-disable-line no-unused-vars
    document.addEventListener('DOMContentLoaded', () => {
        updateUI();
    });

    chrome.storage.onChanged.addListener(() => {
        updateUI();
    });

    document.getElementById('settings').addEventListener('click', () => openSettings());
}


// Prevent this from running when testing
if(!window.jasmine) {
    initPopup();
}