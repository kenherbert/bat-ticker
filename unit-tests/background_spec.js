/*global chrome:true, updateBadge, init */
describe('updateBadge function', () => {
    beforeAll(() => {
        chrome = {
            storage: {
                local: {
                    get: (prefs, callback) => {
                        callback();
                    }
                }
            },
            action: {
                setBadgeBackgroundColor: colour => {}, // eslint-disable-line no-unused-vars
                setBadgeText: text => {}, // eslint-disable-line no-unused-vars
                setTitle: title => {} // eslint-disable-line no-unused-vars
            }
        };

        spyOn(window, 'getCurrency').and.callFake(() => {
            return {
                name: 'US dollars',
                symbol: '$',
                localFormat: '${}'
            };
        });
    });

    it('sets the current price correctly based on the passed data', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098, lastUpdated: Date.now()});
        });

        spyOn(chrome.action, 'setBadgeText').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeText).toHaveBeenCalledWith({text: '6.68'});
    });

    it('sets the badge colour to grey when a large interval has elapsed since last update', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098, lastUpdated: Date.now() - (60 * 6 * 1000)});
        });

        spyOn(chrome.action, 'setBadgeBackgroundColor').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeBackgroundColor).toHaveBeenCalledWith({color: '#555555'});
    });

    it('sets the badge colour to red when update was recent and a negative change is returned', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098, lastUpdated: Date.now()});
        });

        spyOn(chrome.action, 'setBadgeBackgroundColor').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeBackgroundColor).toHaveBeenCalledWith({color: '#990000'});
    });

    it('sets the badge colour to green when a positive change is returned', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: 1.23098, lastUpdated: Date.now()});
        });

        spyOn(chrome.action, 'setBadgeBackgroundColor').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeBackgroundColor).toHaveBeenCalledWith({color: '#009900'});
    });
});


xdescribe('getAPIData function', () => {
    beforeAll(() => {
        chrome = {
            storage: {
                local: {
                    get: (prefs, callback) => {
                        callback();
                    }
                }
            },
            action: {
                setBadgeBackgroundColor: colour => {}, // eslint-disable-line no-unused-vars
                setBadgeText: text => {} // eslint-disable-line no-unused-vars
            }
        };
    });

    xit('sets the current price correctly based on the passed data', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098});
        });

        spyOn(chrome.action, 'setBadgeText').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeText).toHaveBeenCalledWith({text: '6.68'});
    });

    xit('sets the badge colour to red when a negative change is returned', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098});
        });

        spyOn(chrome.action, 'setBadgeBackgroundColor').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeBackgroundColor).toHaveBeenCalledWith({color: '#990000'});
    });

    xit('sets the badge colour to green when a positive change is returned', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: 1.23098});
        });

        spyOn(chrome.action, 'setBadgeBackgroundColor').and.callThrough();

        updateBadge();

        expect(chrome.action.setBadgeBackgroundColor).toHaveBeenCalledWith({color: '#009900'});
    });
});


describe('init function', () => {
    beforeAll(() => {
        chrome = {
            alarms: {
                onAlarm: {
                    addListener: (callback) => {
                        callback();
                    }
                },
                create: (name, args) => { //eslint-disable-line no-unused-vars
                },
                clear: () => {}
            },
            runtime: {
                onMessage: {
                    addListener: (callback) => {
                        callback();
                    }
                }
            }
        };

        spyOn(window, 'getAPIData').and.stub();
        spyOn(window, 'getSupportedCurrencies').and.stub();
        spyOn(chrome.runtime.onMessage, 'addListener').and.stub();
        spyOn(chrome.alarms.onAlarm, 'addListener').and.stub();
    });

    it('triggers a call to getAPIData', () => {
        init();

        expect(window.getAPIData).toHaveBeenCalled();
    });

    it('triggers a call to getSupportedCurrencies', () => {
        init();

        expect(window.getSupportedCurrencies).toHaveBeenCalled();
    });

    xit('creates two timers', () => {
        spyOn(chrome.alarms, 'create').and.callThrough();

        init();

        expect(chrome.alarms.create).toHaveBeenCalledWith('BATTickerTimer', jasmine.any(Object));
        expect(chrome.alarms.create).toHaveBeenCalledWith('CurrencyUpdateTimer', jasmine.any(Object));
    });

    xit('sets a listener for the timer', () => {
        init();

        expect(chrome.alarms.onAlarm.addListener).toHaveBeenCalledWith(jasmine.any(Function));
    });
});
