/*global chrome:true, initOptions, populateUserPrefs, saveUserPrefs */
xdescribe('populateUserPrefs function', () => {
    beforeAll(() => {
        this.fixture = document.createElement('div');
        document.body.appendChild(this.fixture);

        chrome = {
            storage: {
                local: {
                    get: (prefs, callback) => {
                        callback();
                    }
                }
            }
        };


        let headerEl = document.createElement('div');
        headerEl.setAttribute('id', 'header');
        this.fixture.appendChild(headerEl);

        let priceEl = document.createElement('div');
        priceEl.setAttribute('id', 'current-price');
        this.fixture.appendChild(priceEl);

        let changeEl = document.createElement('div');
        changeEl.setAttribute('id', '24h-change');
        this.fixture.appendChild(changeEl);

        let updatedEl = document.createElement('div');
        updatedEl.setAttribute('id', 'last-updated');
        this.fixture.appendChild(updatedEl);


        spyOn(window, 'getCurrency').and.callFake(() => {
            return {
                name: 'US dollars',
                symbol: '$',
                localFormat: '${}'
            };
        });

    });

    it('sets the current price correctly based on the passed data', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098, currency: 'usd', lastUpdated: Date.now()});
        });

        populateUserPrefs();

        expect(document.getElementById('current-price').innerHTML).toBe('$6.68');
    });

    it('sets the 24h change correctly based on the passed data', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: 1.23098});
        });

        populateUserPrefs();

        expect(document.getElementById('24h-change').innerHTML).toBe('+1.23%');
    });

    it('sets the correct sign for 24h change', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098});
        });

        populateUserPrefs();

        expect(document.getElementById('24h-change').innerHTML).toBe('-1.23%');
    });
});


xdescribe('saveUserPrefs function', () => {
    beforeAll(() => {
        this.fixture = document.createElement('div');
        document.body.appendChild(this.fixture);

        let statusEl = document.createElement('span');
        statusEl.setAttribute('id', 'status');
        this.fixture.appendChild(statusEl);

        let currencyEl = document.createElement('select');
        currencyEl.setAttribute('id', 'currency');

        let optionEl = document.createElement('option');
        optionEl.setAttribute('value', 'usd');
        currencyEl.appendChild(optionEl);
        this.fixture.appendChild(currencyEl);
    });

    xit('calls the API method to open the options page', () => {
        saveUserPrefs();

        expect(chrome.runtime.openOptionsPage).toHaveBeenCalled();
    });
});


describe('initOptions function', () => {
    beforeAll(() => {
        this.fixture = document.createElement('div');
        document.body.appendChild(this.fixture);

        let saveEl = document.createElement('button');
        saveEl.setAttribute('id', 'save');
        this.fixture.appendChild(saveEl);

        spyOn(document, 'addEventListener').and.callThrough();
    });

    it('sets the event listener for DOMContentLoaded', () => {
        initOptions();

        expect(document.addEventListener).toHaveBeenCalled();
    });

    xit('sets the event listener for the `save` element click handler', () => {
        initOptions();

        expect(chrome.storage.onChanged.addListener).toHaveBeenCalled();
    });
});
