/*global chrome:true, initPopup, openSettings, updateUI */
describe('updateUI function', () => {
    beforeAll(() => {
        this.fixture = document.createElement('div');
        document.body.appendChild(this.fixture);

        chrome = {
            storage: {
                local: {
                    get: (prefs, callback) => {
                        callback();
                    }
                }
            }
        };


        let headerEl = document.createElement('div');
        headerEl.setAttribute('id', 'header');
        this.fixture.appendChild(headerEl);

        let priceEl = document.createElement('div');
        priceEl.setAttribute('id', 'current-price');
        this.fixture.appendChild(priceEl);

        let changeEl = document.createElement('div');
        changeEl.setAttribute('id', '24h-change');
        this.fixture.appendChild(changeEl);

        let updatedEl = document.createElement('div');
        updatedEl.setAttribute('id', 'last-updated');
        this.fixture.appendChild(updatedEl);


        spyOn(window, 'getCurrency').and.callFake(() => {
            return {
                name: 'US dollars',
                symbol: '$',
                localFormat: '${}'
            };
        });
    });

    it('sets the current price correctly based on the passed data', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098, currency: 'usd', lastUpdated: Date.now()});
        });

        updateUI();

        expect(document.getElementById('current-price').innerHTML).toBe('$6.68');
    });

    it('sets the 24h change correctly based on the passed data', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: 1.23098});
        });

        updateUI();

        expect(document.getElementById('24h-change').innerHTML).toBe('+1.23%');
    });

    it('sets the correct sign for 24h change', () => {
        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 6.678465, change: -1.23098});
        });

        updateUI();

        expect(document.getElementById('24h-change').innerHTML).toBe('-1.23%');
    });
});


describe('openSettings function', () => {
    beforeAll(() => {
        chrome = {
            runtime: {
                openOptionsPage: () => {}
            }
        };

        spyOn(chrome.runtime, 'openOptionsPage').and.callThrough();
    });

    it('calls the API method to open the options page', () => {
        openSettings();

        expect(chrome.runtime.openOptionsPage).toHaveBeenCalled();
    });
});


describe('initPopup function', () => {
    beforeAll(() => {
        this.fixture = document.createElement('div');
        document.body.appendChild(this.fixture);

        chrome = {
            storage: {
                local: {
                    get: (prefs, callback) => {
                        callback();
                    }
                },
                onChanged: {
                    addListener: callback => {
                        callback();
                    }
                }
            }
        };


        let headerEl = document.createElement('div');
        headerEl.setAttribute('id', 'header');
        this.fixture.appendChild(headerEl);

        let priceEl = document.createElement('div');
        priceEl.setAttribute('id', 'current-price');
        this.fixture.appendChild(priceEl);

        let changeEl = document.createElement('div');
        changeEl.setAttribute('id', '24h-change');
        this.fixture.appendChild(changeEl);

        let updatedEl = document.createElement('div');
        updatedEl.setAttribute('id', 'last-updated');
        this.fixture.appendChild(updatedEl);

        let settingsEl = document.createElement('button');
        settingsEl.setAttribute('id', 'settings');
        this.fixture.appendChild(settingsEl);

        spyOn(document, 'addEventListener').and.callThrough();

        spyOn(chrome.storage.local, 'get').and.callFake((args, callback) => {
            callback({price: 0, change: 0, currency: 'usd', lastUpdated: Date.now()});
        });

        spyOn(chrome.storage.onChanged, 'addListener').and.callThrough();
    });

    it('sets the event listener for DOMContentLoaded', () => {
        initPopup();

        expect(document.addEventListener).toHaveBeenCalled();
    });

    it('sets the event listener for chrome.storage.onChanged', () => {
        initPopup();

        expect(chrome.storage.onChanged.addListener).toHaveBeenCalled();
    });
});
